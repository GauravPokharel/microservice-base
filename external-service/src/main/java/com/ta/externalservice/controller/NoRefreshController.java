package com.ta.externalservice.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("no-refresh")
public class NoRefreshController {
    @Value("${mystr:this is default}")
    public String str;

    @Value("${my.greeting:this is default}")
    public String str2;
    @GetMapping("/index")
    public String getAllTask() {
        return str+ " and " + str2;
    }
}
