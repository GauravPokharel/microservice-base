package com.ta.internalservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class InternalServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(InternalServiceApplication.class, args);
    }

}
